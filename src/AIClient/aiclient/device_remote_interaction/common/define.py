# -*- coding: utf-8 -*-
# uncompyle6 version 3.7.5.dev0
# Python bytecode 3.6 (3379)
# Decompiled from: Python 3.7.10 (default, Apr 15 2021, 13:44:35) 
# [GCC 9.3.0]
# Embedded file name: ../../aisdk2/game_ai_sdk/tools/phone_aiclientapi\aiclient\device_remote_interaction\common\define.py
# Compiled at: 2021-02-23 16:10:41
# Size of source mod 2**32: 1503 bytes
GAME_STATE_NONE = 0
GAME_STATE_UI = 1
GAME_STATE_START = 2
GAME_STATE_OVER = 3
GAME_STATE_MATCH_WIN = 4
SUCCESS_CODE = 200
MSG_CMD_CLIENT_DATA = 3000
MSG_CLIENT_CHANGE_GAME_STATE = 3001
MSG_CLIENT_PAUSE = 3002
MSG_CLIENT_RESTORE = 3003
MSG_CLIENT_RESTART = 3004
MSG_CLIENT_REQ = 3100
MSG_CMD_STOP_AI = 3101
MSG_CMD_RESTORE_AI = 3102
MSG_SOURCE_REQ = 3201
MSG_SOURCE_RES = 3202
MSG_AI_ACTION = 2000
MSG_UI_ACTION = 2001
MSG_GAME_STATE = 2002
MSG_AGENT_STATE = 2003
MSG_RESTART_RESULT = 2004
MSG_CLIENT_REP = 2100
MSG_AI_SERVICE_STATE = 5
AGENT_STATE_WAITING = 0
AGENT_STATE_PLAYING = 1
AGENT_STATE_PAUSE = 2
AGENT_STATE_RESTORE_PLAYING = 3
AGENT_STATE_OVER = 4
RESTART_RESULT_SUCCESS = 0
RESTART_RESULT_FAILURE = 1
KEY = '0'
AI_SERVICE_STATE_NORMAL = 0
AI_SERVICE_STATE_AGENT_EXIST = 1
AI_SERVICE_STATE_REG_EXIST = 2
AI_SERVICE_STATE_UI_EXIST = 4
IMG_NONE = -1
DEFAULT_IMG_ID = -1
RAW_IMG_SEND_TYPE = 0
BINARY_IMG_SEND_TYPE = 1
CV2_EN_DECODE_IMG_SEND_TYPE = 2
SERVICE_AI = 1
SERVICE_UI_AI = 2
FRAME_TYPES = [
 0, 1]
FRAME_DECODE_TYPES = [
 0, 1, 2]
FRAME_TYPE_IMG = 0
FRAME_TYPE_DIGITAL = 1